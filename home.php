<!DOCTYPE html>
<html>
<head>
	<title>HAL UTAMA</title>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@1,100;1,500&display=swap" rel="stylesheet">
	<style type="text/css">
		@import url('https://fonts.googleapis.com/css?family=Roboto&display=swap');

		* {
			margin: 0;
			padding: 0;
		}

		nav {
			background-color: #ffffff;
			height: 3em;
			padding: 2em;
		}

		nav .left {
			font-family: Roboto Black;
			float: left;
			font-size: 40pt;

		}
		nav .left a{
			text-decoration: none;
			color: black;
		}

		nav .right{
			font-family: Roboto Thin;
			float: right;
			margin-right: 3em;
		}

		nav .right a {
			text-decoration:none;
			color: black;
		}

		nav .right ul {
			list-style-type: none;

		}
		nav .right ul li {
			display: inline;
			margin-right: 2em;
		}

		nav .right ul li button {
			padding: 1em 2em;
			border: 1px solid transparent;
			background-color: #fff776;
			border-radius: 0.5em;
			color: black;
			font-family: Roboto Black !important;
		}

		body {
			background-color: #48a9dc;

		}
		section {
			color: white;
			width: 100%;
			text-align: center;
			margin: 0 auto;
			margin-bottom: 10em;
		}

		section h1 {
			font-family: Roboto Black;	
			font-size: 42pt;
			margin-bottom: .4em;
		}

		section p {
			font-family: Roboto Thin;
			font-size: 18pt;
			width: 50%;
			margin: 0 auto;
			text-align: left;
		}

		.cell-5 {
			font-family: Roboto Thin;
			font-size: 16pt;
			margin-top: 1em;
			padding: 1em 2em;
			margin: 0 auto;
   			display: grid;
   			color : white;
   			border-radius: 5px;
   			grid-template-columns: 1fr 1fr 1fr 1fr;
   			grid-column-gap: 1rem;
		}

		.cell-5 .cell {
		border-radius: 5px;
   		background-color: #48a9dc;
		}


		section button{
			font-family: Roboto Black;	
			font-size: 12pt;
			display: block;
			margin: 0 auto;
			width: 20%;
			margin-top: 1em;
			padding: 1em 2em;
			border: 1px solid transparent;
			background-color: #fff776;
			border-radius: 0.5em;
			color: black;
		}

		section#home {
			color: white;
			width: 100%;
			text-align: center;
			margin: 0 auto;
			margin-top: 10em;
			margin-bottom: 10em;
		}

		section.home h1 {
			font-family: Roboto Black;	
			font-size: 60pt;
			margin: 0;
		}

		section.home p {
			font-family: Roboto Thin;
			font-size: 18pt;
			width: 100%;
		}

			section.home button {
			font-family: Roboto Black;	
			font-size: 30pt;
			margin-top: 1em;
			padding: 1em 2em;
			width: 40%;
			border: 1px solid transparent;
			background-color: #fff776;
			border-radius: 0.5em;
			color: black;
		}

		input, textarea {
			display: block;
			width: 40%;
			border-radius: 4px;
			border: 1px solid #cecece;
			margin:0 auto;
			margin-bottom: 1em;
		}

		input {
			padding: 1em;
		}

		textarea{
			padding: 1em;
			height: 10em
		}

		footer{
			padding: 2em;
			text-align: center;
			background-color: white;
			font-family: 'Roboto';
		}

	</style>
</head>
<body>
	<nav>
		<div class="left">
			<a href="#home">LOGO</a>
		</div>
		<div class="right">
			<ul>
				<li><a href="#home">Home</a></li>
				<li><a href="#about ">About US</a></li>
				<li><a href="#contact ">Countac US</a></li>
				<li><a href="login.php"><button class="home" onclick="alert('log out Success !')">Log out</button></a></li>
			</ul>
		</div>
	</nav>
	<section id="home">
		<h1>Hello, I am </h1>
		<h1>Andreas Wahyu Kurniawan </h1>
	
	</section>
	<section id="about" style="background-color: white !important; color: black; height:40em">

		<h1>About Me</h1>
		<p>Name 			: Andreas Wahyu Kurniawan</p>
		<p>Email 			: andreaswahyu55@gmail.com</p>
		<p>Phone 			: 082323403911</p>
		<p>Date Of Birth 	: April,11,1999</p>
		<p>Addres 			: Wonocatur, Banguntapan, Bantul</p>
		
		<br><br><br>


		<div class="cell cell-5">
       <div class="cell cell-6"><h2>Educations</h2><br>SD Negeri Banguntapan<br>SMP Muhammadiyah 1 Berbah<br>SMA Negeri 1 Pleret<br><br><br>
      </div>
       <div class="cell cell-7"><h2>Experience</h2><br>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</div>
       <div class="cell cell-8"><h2>Prestasi</h2><br>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</div>
       <div class="cell cell-9"><h2>Diary</h2><br>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</div>
   </div>
  
	</section>
	<section id="contact">
		<h1>Contact Us</h1>

		<form action="kirim.php" method="POST">

		<p><input type="email" name="email" placeholder="Email..."></p>
		<p><textarea name="pesan" placeholder="Pesan..."></textarea>
		<p><input type="submit" name="kirim" value="submit">
			
		</form>

		<br><br><br>
           
	</section>
	<footer>
		<?php 
            include ("counter.php");
            echo "<p style='color:red; font-weight:enchant_broker_list_dicts(broker)'> $kunjungan[0] </p>";
            ?>
		
		<p>Copyright &copy; Wahyu Kruniawan 2020. | All Right Reserved</p>
	</footer>
</body>
</body>
</html>
